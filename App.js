import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import home from './screen/home';
import signup from './screen/register';
import login from './screen/login';
import profile from './screen/userProfile';
import test from './screen/test';

const Stack = createStackNavigator();

const MyStack = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName='Home'
      >
        <Stack.Screen
          name="home"
          component={ home }
        />
        <Stack.Screen
          name="Signin"
          component={ login }
        />
        <Stack.Screen
          name="Signup"
          component={ signup }
        />
        <Stack.Screen
          name="Profile"
          component={ profile }
        />
        <Stack.Screen
          name="Test"
          component={ test }
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default function App() {
  return (
     <MyStack />
  );
}
