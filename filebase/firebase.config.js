import firebase from 'firebase/app'
import { firestore } from 'firebase/firestore'

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCVdiB8AjN7kJs_fYSAGFZSGDBW6C3fPIc",
    authDomain: "bloodtest-47ea5.firebaseapp.com",
    databaseURL: "https://bloodtest-47ea5.firebaseio.com",
    projectId: "bloodtest-47ea5",
    storageBucket: "bloodtest-47ea5.appspot.com",
    messagingSenderId: "115011028121",
    appId: "1:115011028121:web:9585c4ba87d2daffcaa408"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.firestore();
export default firebase;