import React, { Component } from 'react';
import store from '../filebase/firebase.config';

const firestore = store.firestore();

class store extends Component {
    constructor () {
        super();
        this.state.collection_name = "";
        this.state.where = [];
    }
    collection(collection_name) {
        this.state.collection_name = collection_name;
    }
    where(name, expession, value) {
        this.state.where = [name, expession, value];
    }
    get(method = "onSnapshot") {
        window["store"][method]();
    }
    onSnapshot() {
        const dbRef = firestore.collection(this.state.collection_name);
        this.state.where.forEach(item => {
            dbRef.where(item[0], item[1], item[2]);
        });
        dbRef.onSnapshot(query => {
            
        });
    }
}

export default store;