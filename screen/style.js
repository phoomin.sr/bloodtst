import React from 'react'
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        padding: 30,
        flex: 1,
        backgroundColor: '#fff'
    },
    headers: {
        fontSize: 30,
        marginBottom: 12,
        textAlign: 'center'
    },
    text: {
        fontSize: 20,
        marginBottom: 12,
        textAlign: 'center'
    },
    subText: {
        marginBottom: 12,
        fontSize: 12,
    },
    submit: {
        backgroundColor: "#3289a8",
        color: "#fff"
    }
});