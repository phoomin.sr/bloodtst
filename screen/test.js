import React, { Component } from "react";
import { View } from "react-native";
import { Text, Divider, Input, Button } from "react-native-elements";
import style from "./style";

class test extends Component {
    constructor(props){
        super(props);
        const uinfo = this.props.route.params;
        this.state = {
            fullname: uinfo.fullname,
            email: uinfo.email,
            age: uinfo.age,
            user_id: uinfo.user_id
        }
    }
    saveBloodTestResult() {
        console.log(this.state);
    }
    render(){
        return (
            <View style={style.container}>
                <Text style={style.headers}>แบบทดสอบ</Text>
                <Text style={style.subText}>ยินดีต้อนรับ คุณ { this.state.fullname } เข้าสู่การทดสอบสมรรถภาพตับจากผลการตรวจเลือดของคุณ ระบบสามารถนำผลตรวจเลือด
                ของคุณไปใช้ในการกรองข้อมูลข่าวสารที่เหมาะสมกับคุณที่สุดเพียงตอบคำถามเหล่านี้</Text>
                <Text style={style.text}>ระดับ AST และ ALT</Text>
                <Input
                    name='ast'
                    placeholder="AST : IU/L"
                    onChange={(e) => {
                        this.setState({ast: e.target.value})
                    }}
                />
                <Input
                    name='alt'
                    placeholder="ALT : IU/L"
                    onChange={(e) => {
                        this.setState({alt: e.target.value})
                    }}
                />
                <Divider
                    orientation="horizontal"
                />
                <Text style={style.text}>ระดับ GGT</Text>
                <Input
                    name='ggt'
                    placeholder="GGT : IU/L"
                    onChange={(e) => {
                        this.setState({ggt: e.target.value})
                    }}
                />
                <Divider
                    orientation="horizontal"
                />
                <Text style={style.text}>ระดับ APL</Text>
                <Input
                    name='apl'
                    placeholder="APL : IU/L"
                    onChange={(e) => {
                        this.setState({apl: e.target.value});
                    }}
                />
                <Divider
                    orientation="horizontal"
                />
                <Text style={style.text}>ระดับ T-BIL</Text>
                <Input
                    name='tbil'
                    placeholder="T-BIL : IU/L"
                    onChange={
                        (e) => {
                            this.setState({tbil: e.target.value});
                        }
                    }
                />
                <Divider
                    orientation="horizontal"
                />
                <Text style={style.text}>ระดับ LDH</Text>
                <Input
                    name='ldh'
                    placeholder="LDH : IU/L"
                    onChange={
                        (e) => {
                            this.setState({ldh: e.target.value})
                        }
                    }
                />

                <Button style={style.submit}
                    title="ยืนยันผลตรวจ"
                    onPress={() => this.saveBloodTestResult()}
                />
            </View>
        );
    }
}

export default test;