import React, { Component, useState } from 'react'
import { View, Text, StyleSheet, FlatList } from 'react-native'
import { Button, Header, Overlay } from 'react-native-elements';
import commonStyle from './style';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import styles2 from './style'

const styles = StyleSheet.create({
    Flatlist: {
        margin: 5,
        backgroundColor: '#fff',
        padding: 15,
        borderRadius: 10,
        borderStyle: 'solid'
    },
    TitleFlatlist: {
        fontWeight: 'bold',
        color: '#3289a8',
    }
});

const DATA = [
    {
        id: '1',
        title: 'Liver Performance',
        contents: 'description for Liver Performance',
        img: ''
    },
    {
        id: '2',
        title: 'Liver Performance',
        contents: 'description for Liver Performance',
        img: ''
    },
    {
        id: '3',
        title: 'Liver Performance',
        contents: 'description for Liver Performance',
        img: ''
    },
    {
        id: '4',
        title: 'Liver Performance',
        contents: 'description for Liver Performance',
        img: ''
    },
    {
        id: '5',
        title: 'Liver Performance',
        contents: 'description for Liver Performance',
        img: ''
    },
    {
        id: '6',
        title: 'Liver Performance',
        contents: 'description for Liver Performance',
        img: ''
    },
    {
        id: '7',
        title: 'Liver Performance',
        contents: 'description for Liver Performance',
        img: ''
    },
    {
        id: '8',
        title: 'Liver Performance',
        contents: 'description for Liver Performance',
        img: ''
    }
]

const getContent = ({id}) => {
    
}

const Item = ({ title, descript }) => {
    return (
        <View style={ styles.Flatlist }>
            <Text style={styles.TitleFlatlist}>{title}</Text>
            <Text>{descript}</Text>
        </View>
    );
}


class home extends Component {
constructor (props) {
    super(props);
}
render () {
    const renderItem = ({item}) => (<Item title={item.title} descript={item.contents} onPress={() => clickLinkId()} />);
    return (
        <View style={commonStyle.container}>
            <FlatList 
                data={DATA}
                renderItem={renderItem}
                keyExtractor={ item => item.id }
            />
            <Button style={styles2.footerButton}
                title="Signin"
                color='blue'
                onPress={() => this.props.navigation.navigate('Signin')}
            />
        </View>
    );
}
}

export default home;