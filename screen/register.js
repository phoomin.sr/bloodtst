import React, { Component } from 'react'
import { Alert, StyleSheet, SafeAreaView } from 'react-native';
import { Input, Button, Text } from 'react-native-elements';
import firebase from './filebase';

const styles = StyleSheet.create({
    container: {
        padding: 5,
        backgroundColor: '#fff',
        textAlign: 'center',
        flex: 1
    },
    SignupButton: {
        backgroundColor: '#fa5f71',
        color: '#fff',
        bottom: 0
    },
    headers: {
        fontSize: 30,
        marginBottom: 12
    },
    text: {
        fontSize: 16,
        marginBottom: 12,
        padding: 2
    },
    input: {
        marginTop: 10
    },
    submit: {
        marginTop: 12,
        backgroundColor: "#3289a8",
        color: "#fff"
    }
})

class register extends Component {
    constructor () {
        super();
        this.state = {
            fullname: "",
            email: "",
            age: 0,
            password: "",
            confirmPassword: "",
            isLoading: false
        }
        this.error = false;
        this.dbRef = firebase.firestore().collection('users');
    }
    submitEvent = () => {
        if (this.state.fullname == "") {
            Alert.alert("Alert", "please fill user full name!")
            return;
        }
        if (this.state.password !== this.state.confirmPassword) {
            Alert.alert("Alert", "password not match with confirm password.");
            return;
        }

        this.state.isLoading = true
        this.dbRef.add({
            fullname: this.state.fullname,
            email: this.state.email,
            age: this.state.age,
            password: this.state.password
        }).then((res) => {
            this.setState({
                fullname: "",
                email: "",
                age: "",
                password: "",
                isLoading: false
            });
            this.props.navigation.navigate('Signin');
        })
    }
    render(){
        return (
            <SafeAreaView style={styles.container}>
                <Text style={styles.headers}>Register</Text>
                <Input 
                    name="fullname"
                    labal="Fullname"
                    placeholder="fullname"
                    style={styles.input}
                    onChange={(e) => {
                        console.log(e.target.value);
                        this.setState({fullname: e.target.value}
                    )}}
                />
                <Input
                    name="age"
                    labal="age"
                    placeholder="age"
                    keyboardType="numeric"
                    style={styles.input}
                    onChange={(e) => {
                        this.setState({age: e.target.value})
                    }}
                />
                <Input
                    name="email"
                    labal="email"
                    placeholder="email"
                    style={styles.input}
                    onChange={(e) => {
                        this.setState({email: e.target.value})
                    }}
                />
                <Input 
                    name="password"
                    labal="password"
                    placeholder="password"
                    style={styles.input}
                    onChange={(e) => {
                        console.log(e.target.value);
                        this.setState({password: e.target.value})
                    }}
                    secureTextEntry={true}
                />
                <Input 
                    name="confirm-password"
                    labal="confirm-password"
                    placeholder="confirm-password"
                    style={styles.input}
                    onChange={(e) => {
                        this.setState({confirmPassword: e.target.value})
                    }}
                    secureTextEntry={true}
                />
                <Button
                    style={styles.submit}
                    title="register"
                    loading={this.state.isLoading}
                    onPress={e => {this.submitEvent()}}
                />
            </SafeAreaView>
        );
    }
}

export default register;