import React, { Component } from 'react'
import { StyleSheet, SafeAreaView, View } from 'react-native'
import { Button, Input, Text } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome'
import firebase from './filebase'

const styles = StyleSheet.create({
    container: {
        padding: 5,
        backgroundColor: '#fff',
        textAlign: 'center',
        flex:1
    },
    SignupButton: {
        backgroundColor: '#fa5f71',
        color: '#fff'
    },
    headers: {
        fontSize: 30,
        marginBottom: 12,
        textAlign: 'center'
    },
    text: {
        fontSize: 20,
        marginBottom: 12,
        textAlign: 'center'
    },
    submit: {
        backgroundColor: "#3289a8",
        color: "#fff"
    }
});

class login extends Component {
    constructor () {
        super();
        this.dbRef = firebase.firestore().collection('users');
        this.state = {
            userName: "",
            UserPass: "",
            isLoading: false,
            uinfo: {}
        }
    }
    stateUserName = (value) => {
        this.state.userName = value;
    }
    stateUserPass = (value) => {
        this.state.UserPass = value;
    }
    LoginSubmit = () => {
        firebase.firestore().collection('users')
        .where('email', '==', this.state.userName)
        .where('password', '==', this.state.UserPass)
        .onSnapshot(querySnapshot => {
            querySnapshot.forEach((value) => {
                if (value.exists == true) {
                    this.state.userId = value.id
                    firebase.firestore().collection('users').doc(this.state.userId)
                    .onSnapshot((res) => {
                        const uinfo = res.data();
                        this.props.navigation.navigate('Test', {
                            fullname: uinfo.fullname,
                            email: uinfo.email,
                            age: uinfo.age,
                            userId: this.state.userId
                        });
                    });
                } else {
                    Alert.alert('Sorry!', 
                    "Your username or password is't valid.",
                    [
                        {
                            text: "Ok",
                            onPress: () => console.log('Ok'),
                        },
                        {
                            text: "register",
                            onPress: () => this.props.navigation.navigate('Signup')
                        }
                    ]);
                }
            });
        });
    }

    render() {
        return (
            <View style={ styles.container }>
                <Text style={ styles.Headers }>Sign In</Text>
                <Input
                    label="Username or Email"
                    placeholder="username or email"
                    style={styles.input}
                    onChange={e => this.stateUserName(e.target.value)}
                />
                <Input
                    label="Password"
                    placeholder="Enter password"
                    style={styles.input}
                    onChange={e => this.stateUserPass(e.target.value)}
                    secureTextEntry={true}
                />
                <Button
                    title="Sign In"
                    style={styles.buttonPrimary}
                    onPress={() => this.LoginSubmit()}
                />
                <Text style={styles.textcenter}>-- or --</Text>
                <Button
                    title="Register"
                    style={styles.buttonSecoundary}
                    onPress={ () => this.props.navigation.navigate('Signup') }
                />
            </View>
        );
    }
}
    

export default login;